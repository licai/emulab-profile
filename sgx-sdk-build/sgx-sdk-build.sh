#!/bin/bash
#set -u
#set -x
# deploy sgx on emulab

SCRIPTDIR=$(dirname "$0")
WORKINGDIR='/mnt/extra'
SGXSRC='/mnt/extra/sgxsrc'
SGXSDKROOT='/mnt/extra/sgxsdk'
username=`whoami`
usergid=`id -g $username`

sudo chown ${username}:${usergid} /mnt/extra/ -R
cd $WORKINGDIR
exec >> ${WORKINGDIR}/deploy.log
exec 2>&1

sudo apt-get update
sudo apt-get -y install build-essential ocaml automake autoconf libtool wget python vim screen

git clone https://github.com/01org/linux-sgx.git $SGXSRC
cd $SGXSRC
./download_prebuilt.sh
sudo make DEBUG=1 sdk_install_pkg
cd $WORKINGDIR
printf "yes\n" | sudo $SGXSRC/linux/installer/bin/sgx_linux_x64_sdk_*.bin
source $SGXSDKROOT/environment
cd $SGXSDKROOT/SampleCode/RemoteAttestation
sudo make SGX_MODE=SIM SGX_SDK=$SGX_SDK

echo "\n\n\n\nPlease checkout these directories:"
echo "    SGX source dir: $SGXSRC"
echo "    SGX SDK dir: $SGXSDKROOT"
echo "    To finish run sgx program run:"
echo "      \`source $SGXSDKROOT/environment\`"
echo "    Try executing commands below to verify it works: "
echo "      cd $SGXSDKROOT/SampleCode/RemoteAttestation"
echo "      ./app"
