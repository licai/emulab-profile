""" A profile for working with Intel SGX
It just scripts out the building instruction of [building linux-sgx-sdk](https://github.com/01org/linux-sgx)

For more information, please read the build script at /mnt/extra/pub-sgx-build.sh after it is instantiated.
1. Deliberate not specify the hardware type
2. Has local storage at /mnt/extra and /opt
3. ubuntu 16.04<br>

Instructions:
###Note:
when you instantiate the profile, after the host is booted.<br>
You can see the experiment "State" is **"booted (startup services are still running)"** for a while.<br>
This is because the SGX building procedure is running.<br>
So make sure you login after the "State" change into **"ready"**

Once it is instantiated, login and run:\n
```shell-script
    source /mnt/extra/sgxsdk/environment
```

You can find \n
    SGX source is at: /mnt/extra/sgxsrc
    SGX sdk is at: /mnt/extra/sgxsdk

You can also try out the remote attestation code: 
```shell-script
    source /mnt/extra/sgxsdk/environment \n
    cd /mnt/extra/sgxsdk/SampleCode/RemoteAttestation \n
    ./app
```
"""
