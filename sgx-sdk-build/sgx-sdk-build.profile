""" Intel SGX SDK prebuilt
It just scripts out the building instruction of [building linux-sgx-sdk](https://github.com/01org/linux-sgx)

For more information, please read the build script at /mnt/extra/sgx-sdk-build.sh after it is instantiated.
1. Deliberate not specify the hardware type
2. Has local storage at /mnt/extra
3. ubuntu 16.04<br>

Instructions:
###Note:
when you instantiate the profile, after the host is booted.<br>
You can see the experiment "State" is **"booted (startup services are still running)"** for a while.<br>
This is because the SGX building procedure is running.<br>
So make sure you login after the "State" change into **"ready"**

Once it is instantiated, login and run:\n
```shell-script
    source /mnt/extra/sgxsdk/environment
```

You can find \n
    SGX source is at: /mnt/extra/sgxsrc
    SGX sdk is at: /mnt/extra/sgxsdk

You can also try out the remote attestation code: 
```shell-script
    source /mnt/extra/sgxsdk/environment \n
    cd /mnt/extra/sgxsdk/SampleCode/RemoteAttestation \n
    ./app
```
"""

#
# NOTE: This code was machine converted. An actual human would not
#       write code like this!
#

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import the Emulab specific extensions.
import geni.rspec.emulab as emulab

# Create a portal object,
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Node h
node_h = request.RawPC('h')
node_h.disk_image = 'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU16-64-STD'
node_h.Desire('?+disk_nonsysvol','190734')
bs0 = node_h.Blockstore('bs', '/mnt/extra')
bs0.size = '100GB'
bs0.placement = 'NONSYSVOL'

node_h.addService(pg.Install('https://gitlab.flux.utah.edu/licai/emulab-profile/raw/master/sgx-sdk-build/sgx-sdk-build.tar.gz','/mnt/extra/'))
node_h.addService(pg.Execute(shell="bash", command="/mnt/extra/sgx-sdk-build.sh"))

request.setRoutingStyle('static')

# Print the generated rspec
pc.printRequestRSpec(request)

