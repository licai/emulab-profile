Emulab profiles. 

Each directory is one profile, including:
1. geni script: <profile_name>.profile
2. init installation script:  <profile_name>.sh
3. profile instruction: <profile_name>.md  ---- this should be the same as the one in geni script. but would be better to show here for browsing without instantiateing it.
4. profile tar ball: <profile_name>.tar.gz ---- this is to be downloaded by emulab.

packing.sh 
will pack the installation script to a tar package, and push to the public accessible repo
so that the emulab will fetch it when it is instantiated.
