""" Tensorflow and ssdeep installed.
Local storage at /mnt/extra and /opt
ubuntu 16.04
using d430. Because the feature alignment script eat up lots of memory.

Instructions:
Everything is in /mnt/extra
"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import the Emulab specific extensions.
import geni.rspec.emulab as emulab

# Create a portal object,
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Node h
node_h = request.RawPC('h')
node_h.hardware_type = 'd430'
node_h.disk_image = 'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU16-64-STD'
node_h.Desire('?+disk_nonsysvol','190734')
bs0 = node_h.Blockstore('bs1', '/mnt/extra')
bs0.size = '100GB'
bs0.placement = 'NONSYSVOL'

node_h.addService(pg.Install('https://gitlab.flux.utah.edu/licai/emulab-profile/raw/master/tensorflow/tensorflow.tar.gz','/mnt/extra/'))
node_h.addService(pg.Execute(shell="bash", command="/mnt/extra/tensorflow.sh"))

request.setRoutingStyle('static')

# Print the generated rspec
pc.printRequestRSpec(request)

#
# Here is the original NS file from the Emulab experiment.
#
# set ns [new Simulator]
# $ns description "Used for data analysis"
# source tb_compat.tcl
# 
# set h [$ns node]
# 
# tb-set-hardware $h d430
# 
# tb-set-node-os $h UBUNTU14-64-STD
# 
# set bs1 [$ns blockstore]
# $bs1 set-class "local"
# $bs1 set-size "100GB"
# $bs1 set-placement "nonsysvol"
# $bs1 set-mount-point "/mnt/extra"
# $bs1 set-node $h
# 
# set bs2 [$ns blockstore]
# $bs2 set-class "local"
# $bs2 set-size "100GB"
# $bs2 set-placement "nonsysvol"
# $bs2 set-mount-point "/opt"
# $bs2 set-node $h
# 
# 
# $ns rtproto Static
# 
# # Go!
# $ns run
