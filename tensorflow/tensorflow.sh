#!/bin/bash
#set -u
#set -x
# deploy sgx on emulab

SCRIPTDIR=$(dirname "$0")
WORKINGDIR='/mnt/extra'
VENV='/mnt/extra/venv'
username=`whoami`
usergid=`id -g $username`


sudo chown ${username}:${usergid} /mnt/extra/ -R
cd $WORKINGDIR
exec >> ${WORKINGDIR}/deploy.log
exec 2>&1

# install ssdeep
sudo apt-get -y install build-essential libffi-dev python python-dev python-pip automake autoconf libtool
sudo pip install simplejson
mkdir ssdeep
cd ssdeep
wget https://svwh.dl.sourceforge.net/project/ssdeep/ssdeep-2.13/ssdeep-2.13.tar.gz
tar xf ssdeep-2.13.tar.gz
cd ssdeep-2.13/
./configure;
make;
sudo make install
sudo BUILD_LIB=1 pip install ssdeep

cd $WORKINGDIR
mkdir $VENV
cd $VENV


# install tensorflow
sudo apt-get update
sudo apt-get -y install python-pip vim screen  python-pip python-dev python-virtualenv 

virtualenv --system-site-packages $VENV
source ${VENV}/bin/activate

#sudo pip install tensorflow
pip install --upgrade https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-1.2.1-cp27-none-linux_x86_64.whl
pip install keras


echo ""
echo "Because the installation of tensorflow somehow interferecen with ssdeep"
echo "So I install it in the virtualenv"
echo "So please run \`source ${VENV}/bin/activate\` before you use tensorflow"



