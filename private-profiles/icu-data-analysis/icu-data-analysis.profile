""" For icu project data analysis
Local storage at /mnt/extra and /opt
ubuntu 16.04
using d430. Because the feature alignment script eat up lots of memory.

Instructions:
Everything is in /mnt/extra
"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import the Emulab specific extensions.
import geni.rspec.emulab as emulab

# Create a portal object,
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Node h
node_h = request.RawPC('h')
node_h.hardware_type = 'd430'
node_h.disk_image = 'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU16-64-STD'
node_h.Desire('?+disk_nonsysvol','190734')
bs0 = node_h.Blockstore('bs1', '/mnt/extra')
bs0.size = '100GB'
bs0.placement = 'NONSYSVOL'

node_h.addService(pg.Install('https://gitlab.flux.utah.edu/licai/emulab-profile/raw/master/private-profiles/icu-data-analysis/icu-data-analysis.tar.gz','/mnt/extra/'))
node_h.addService(pg.Execute(shell="bash", command="/mnt/extra/icu-data-analysis.sh"))

request.setRoutingStyle('static')

# Print the generated rspec
pc.printRequestRSpec(request)
