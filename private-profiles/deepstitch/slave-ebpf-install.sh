#!/bin/bash
#set -u
#set -x
# deploy sgx on emulab
SCRIPTDIR=$(dirname "$0")
WORKINGDIR='/mnt/extra/'
username=$(id -u)
usergid=$(id -g)

sudo chown ${username}:${usergid} ${WORKINGDIR}/ -R
cd $WORKINGDIR
exec >> ${WORKINGDIR}/deploy-ebpf.log
exec 2>&1

KUBEHOME="${WORKINGDIR}/kube/"
mkdir -p $KUBEHOME && cd $KUBEHOME
export KUBECONFIG=$KUBEHOME/admin.conf

pushd $WORKINGDIR
git clone https://github.com/iovisor/bcc.git
git clone https://github.com/pmem/vltrace.git

# install libbcc 0.4.0. HIGHER version does not work.
# https://github.com/iovisor/bcc/blob/master/INSTALL.md
# All versions
sudo apt-get -y install bison build-essential cmake flex git libedit-dev \
  libllvm3.7 llvm-3.7-dev libclang-3.7-dev python zlib1g-dev libelf-dev

# For Lua support
sudo apt-get -y install luajit luajit-5.1-dev

mkdir bcc/build
cd bcc/build
git checkout tags/v0.4.0 -b 0.4.0
cmake .. -DCMAKE_INSTALL_PREFIX=/usr
make
sudo make install
popd

# install vltrace
mkdir -p $WORKINGDIR/vltrace/build
cd $WORKINGDIR/vltrace/build
cmake ..
make
sudo make install


