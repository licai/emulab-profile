#!/bin/bash
#set -u
#set -x
# deploy sgx on emulab
SCRIPTDIR=$(dirname "$0")
WORKINGDIR='/mnt/extra/'
username=$(id -u)
usergid=$(id -g)

sudo chown ${username}:${usergid} ${WORKINGDIR}/ -R
cd $WORKINGDIR
exec >> ${WORKINGDIR}/deploy.log
exec 2>&1

KUBEHOME="${WORKINGDIR}/kube/"
mkdir -p $KUBEHOME && cd $KUBEHOME
export KUBECONFIG=$KUBEHOME/admin.conf

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list

cd $WORKINGDIR
git clone git@gitlab.flux.utah.edu:licai/deepstitch.git

sudo apt-get update
sudo apt-get -y install build-essential libffi-dev python python-dev  \
python-pip automake autoconf libtool indent vim tmux jq exuberant-ctags \
python3-dev

curl https://bootstrap.pypa.io/get-pip.py | sudo python3

# learn from this: https://blog.csdn.net/yan234280533/article/details/75136630
sudo apt-get -y install kubelet kubeadm kubectl kubernetes-cni golang-go jq
#sudo apt-get -y install docker-engine
# this to install docker-ce, instead of docker-engine
# curl -fsSL get.docker.com | sh
sudo apt -y install docker.io
sudo systemctl enable docker.service
sudo docker version
sudo swapoff -a

master_token=''
#patched=1
while [[ -z $master_token ]]
#while [[ -z $master_token ]] || [[ $patched == 1 ]]
do
    #patched=`ssh -o StrictHostKeyChecking=no m "cat /var/lib/kubelet/config.yaml |grep resolv.conf |wc -l"`
    master_token=`ssh -o StrictHostKeyChecking=no m "export KUBECONFIG='/mnt/extra/kube/admin.conf' &&   kubeadm token list |grep authentication | cut -d' ' -f 1"`;
    sleep 1;
done
sudo kubeadm join m:6443 --token $master_token --discovery-token-unsafe-skip-ca-verification 
scp m:$KUBEHOME/admin.conf $KUBEHOME/admin.conf

# patch the kubelet to force --resolv-conf=''
sudo sed -i 's#Environment="KUBELET_CONFIG_ARGS=--config=/var/lib/kubelet/config.yaml"#Environment="KUBELET_CONFIG_ARGS=--config=/var/lib/kubelet/config.yaml --resolv-conf=''"#g' /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
sudo systemctl daemon-reload 
sudo systemctl restart kubelet.service

# if it complains that "[ERROR Port-10250]: Port 10250 is in use", kill the process.
# if it complains some file already exist, remove those. [ERROR FileAvailable--etc-kubernetes-pki-ca.crt]: /etc/kubernetes/pki/ca.crt already exists

date

# because on ubuntu 18.04 docker-engine somehow does not work very well with kubernetes, I may either choose docker-ce or fall back to ubuntu1604. 
# But somehow the 18.04 configuration using daemon does not work properly somehow. The four microservices can not boot up correctly.
# so just roll back.
sudo apt-get -y install linux-image-4.13.0-16-generic linux-tools-4.13.0-16-generic linux-headers-4.13.0-16-generic
sudo pip install kubernetes docker psutil
sudo pip3 install kubernetes docker psutil numpy matplotlib keras tensorflow

