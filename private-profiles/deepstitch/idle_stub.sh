#!/bin/bash
#set -u
#set -x
# deploy sgx on emulab

SCRIPTDIR=$(dirname "$0")
WORKINGDIR='/mnt/extra'
VENV='/mnt/extra/venv'
username=`whoami`
usergid=`id -g $username`

sudo chown ${username}:${usergid} /mnt/extra/ -R
cd $WORKINGDIR
exec >> ${WORKINGDIR}/deploy.log
exec 2>&1

# install ssdeep
sudo apt-get update
sudo apt-get -y install build-essential libffi-dev python python-dev  \
python-pip automake autoconf libtool indent vim tmux 
sudo apt-get -y install python-pip vim screen python-pip python-dev python-virtualenv ctags
sudo BUILD_LIB=1 pip install ssdeep
sudo pip install simplejson
sudo pip install fuzzyhashlib

cd $WORKINGDIR
mkdir $VENV
cd $VENV

# clone some git repo that we need for icu
cd $WORKINGDIR
git clone git@gitlab.flux.utah.edu:licai/deepstitch.git
git clone git@gitlab.flux.utah.edu:licai/icu.git
git clone git@gitlab.flux.utah.edu:licai/emulab-profile.git

