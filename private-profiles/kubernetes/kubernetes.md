This is a profile deploys the following components:
1. Kubernetes, multi-node clusters using kubeadm, using docker.
2. Sockshop demo microservices
3. Jaeger application tracing

Out of convenience, it is also instantiated with:
1. kubernetes dashboard installed.
2. helm, to install kubernetes "packages"
3. jid and jq, for json format parsing.

You can find the deploy script at:
    /mnt/extra/master.sh for master node
    /mnt/extra/slave.sh for slave node

The deployment log is kept at /mnt/extra/deploy.log

You can find endpoint ip and port using the following commnd:
```bash
    export KUBEHOME="/mnt/extra/kube/"
    export KUBECONFIG=$KUBEHOME/admin.conf
    jaeger_port=`kubectl --namespace=jaeger get svc -o go-template='{{range .items}}{{if eq .metadata.name "jaeger-query"}}{{index .spec.ports 0 "nodePort"}}{{"\n"}}{{end}}{{end}}'`
    jaeger_endpoint=`kubectl get endpoints --all-namespaces |grep jaeger-query|awk '{print $3}'`
    sockshop_endpoint=`kubectl get endpoints --all-namespaces |grep front-end|awk '{print $3}'`
    dashboard_endpoint=`kubectl get endpoints --all-namespaces |grep dashboard|awk '{print $3}'`
    dashboard_credential=`kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}') |grep token: | awk '{print $2}'`

    echo "Kubernetes is ready at: http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/login"
    echo "sockshop will be ready in 5 minutes at: http://localhost:30001"
    echo "jaeger will be ready in 5 minutes at: http://localhost:${jaeger_port}"

    # optional address
    echo "Or, another access option (jaeger's localhost port does not work on my windows port forwarding somehow)"
    echo "Jaeger endpoint: $jaeger_endpoint"
    echo "sockshop endpoint: $sockshop_endpoint"
    echo "kubernetes dashboard endpoint: $dashboard_endpoint"
    # dashboard credential
    echo "And this is the dashboard credential: $dashboard_credential"
```
