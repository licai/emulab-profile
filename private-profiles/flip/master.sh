#!/bin/bash
#set -u
#set -x

SCRIPTDIR=$(dirname "$0")
WORKINGDIR='/mnt/extra/'
username=$(id -nu)
usergid=$(id -ng)
experimentid=$(hostname|cut -d '.' -f 2)
projectid=$usergid

KUBEHOME="${WORKINGDIR}/kube/"
sudo mkdir -p ${WORKINGDIR}
sudo chown ${username}:${usergid} ${WORKINGDIR}/ -R
mkdir -p $KUBEHOME && cd $KUBEHOME

sudo chown ${username}:${usergid} ${WORKINGDIR}/ -R
cd $WORKINGDIR
exec >> ${WORKINGDIR}/deploy.log
exec 2>&1


DEPLOY_CONFIG="${WORKINGDIR}/deepstitch/kubernetes/deploy-config-in/"
export KUBECONFIG=$KUBEHOME/admin.conf

cd 
mkdir .ssh
cat >> .ssh/id_rsa << EOF
-----BEGIN RSA PRIVATE KEY-----
MIIJKQIBAAKCAgEAw+08BV+AA2MldZg1vpFMru1jEs/hdiX5j7BJF3h/y8AZP3l7
BR+KvRjy8CK+zD5ssMUttj6XHE3Y+sxbybLXqiH7gKXQNSqOMdo5l6HdWlr5LaE1
P/VSpHIjAlnjqa/Wkkjb6giL+Jlfs9qs9N+PdHKE1U3duYrJnXbu9bmoZqJVlH30
G9Iq3EPZ21kkeq0llvOrNJ6RH+VL4mlqAWiTKLTG1KHOpotWE8jIseXWY1XY+H1w
oNPdEEEy8XVompuNbqE1bnO/t5lT/Esz4uY01pDJTvkhqiTPPs1wmgJAhLV1xHOk
GI/57tWOIjvQi09qK1ImNsgMeBVuZoA/jl3OuFYbuwQR8Vv62AX5X4huZYZ4G+lW
jilNzWU3bbyB0UOOrAQOGTsiFI+7SojwztORjKq0DPf+wXbTgFKEIPzF+Me8VFoz
C3YDOX6HQRx6bAmSaKcZBeTIkg55djRMQb352juWdcWQPgymFWctLcEnzFRkONhe
S6rAH3XXekqdsxhLAZLThhbQoFkaG6sFk5xGIyU9tq2zwI0iSKcVJabxcTX8gwDq
FCGhH31BwazIKSV41GedSd+9mi+QaZbeSZ69AkYuIPxS+zE6c3OwpHYqUAdHRgP0
FAq70KsLjv3mXZkmQkrX/alykOvxHgnhFQaG9Aw6aGxR7IhOOvaX4HADrmUCAwEA
AQKCAgBs9FeFcWdB+vfQ3lU85ZZsTMzkoD/0MQgIVPNAnx0KVIh12PuFjNXddKtc
ySWrdSeCYhSanVHxRoVcaxq8Hr15+i3fi9YuSr0Kkh2m6jerUJ3zNwvixm0vz90S
KKTHL2a2sNfC3P/hvoEPBpNaFZy0GB8wc33S+ZBHBeW7NoSklq/axPRkKEr3dLAJ
IBh+NB2MRXM27tsrjprVjOOLZvk3ENjlVBoUWbYDiIu7CmJfmVTwUum4NjlgGbHX
vv/twoUzOWj6RFogIp8m1TdEfdpyW8/nH/oNoSyy8xIkQw5X/oVAO0LM+fv2SkIh
XXwKjJ4w+x0PJ85vVROw4q71X8tdlCZ98/x9rGRXggK0QIHzStdbEYq9xqMo4lsE
WGzPGWQxgr8jo7kO396jCw8KJONqHBkJSQ0cYnQ86v10ev21A+q0KzGP5YPxuqOd
g476Gxgmt0clvXVTlgoMsuyrUNDHqnKAgcc+/uulnf37U596ezmYKJnL8ewsJrA8
R0GFs9mRrngw+WqjJsm5Y/M7EOdEk+mrPlMnYNK5GpjO5SOyyeCB2yuB8UOtMJll
769i+gDIRJcoqEKdSXoPSrSr5/UaXbDuhBknYiZM9vcyJ+dD2vQ22H8kUPKcLJXU
5PUZ0z/oFP4ZxHTgma8Ad5Kj+Fxn9FR8P7jy4yQllS8s4oA3HQKCAQEA5yPhdSqj
VGzVuAE+BqDh+Q05caJg1c9QXZJit9DnS7bjyqmBeSzD47U15Zxkh3iNPwn+cS/i
isv7E6a7G9bSrl5+5MnlPFRVlUFZ1aBXMswdpOP+V9eh20/DlOsgNc/dlxuSOVcM
zL6wJdy6K8TdAscCJKvI4eNpCWnpYaKNgD2lNqGb7cQ4FInxVWKJhl/Z8PQz2OOO
kt0taD3V3QKXKsTBUthjxuBAIfBbrE93bjw1lWmZTfuBm9hjL5q0PYYfjHY3M7IK
GPvMHncNtB2PN62X+3gcAUGWVwWKLe23x7GLc6PO1xe3vC/C9FX4OSuXCN3UkBYJ
NgpMEFt+sIkZPwKCAQEA2P/M9AsGkoJM25r8ryvUkJ+BEdFjiXK0e6MrbqpUGLxN
xv2rmF6edZpgvcEoHzrI2GYveEk+qoCjOT4ndvqDmhpAqfmv9sVdw68HHA52vjAS
sSJCrwlchM05ksLfSFAG6ty3ZCFqsWs3as1weXzE+LNA0Mc1Ja4iUUiP9yLpX0Xj
c2zYZhBqByaVBYjcGoiCJhp35QBerik/pmv4W6BkGFR+qd2zjVocblCEQwnPrbpE
1JSEicihu4exSrJNkOqOuTe0XSNtskLqpQ4HzJG5e6qqextJuMamtFq/1d1cI1U1
Pkp6ytzKSocTAs8TXll4uczdG6ltaT3WizlTZk4LWwKCAQEAwTVu+r+/c4zeQUdY
j7Xf7o0tXV18UH1REkaRGlIXZWTlmLyEK8RNooQRH3PRXKRafT1fYUHDvn2vzMds
VGMIDRWUQK67Xeio+XB9JWM4QI/6n6CtNMRc2bQdKtg4PoMZGXzEiAXJ/0POfne5
b2jJzc6Ibhriel/bpzDPEvwMCAx/k+EXNHL25cRA5TzO9HIe/ZlD1aKxxPX5RM+b
e3j0351EdHnJPa6qLTR7jNeIX68Yo2J38YWw25LPpjVNQLhimskeb6n9YtDn2Pdv
jwW+d642AtBtqGcowmkxDh9NpjE0/l8JQSbkQPwznIZYTYGwaIr1adPhszh8SJgS
mIKhhQKCAQEA0LzwKGlavyA8Ups6Kj8dUKk+fSdQem0ulCgXpXKPRuQMZTirEf0i
rLn+CUp/D9t6TD+vtiOu9lCbsnrJd7fvVBOg1GDtFZIxagU575EyRCz2LhJo1vtI
Vi8cDMMMCgGBRfdfKiDDAzvBqSlJ4nNU/m9gD/UweLiTJmGIqxCYMR+Hdgbj+vob
JBnHC/Xk1Fkv6PtZDZ5XdW0UX0BJPcqBvq240ioDiBuAcdVeBSaw8crWXy8md2vZ
uA0FTHdMCfS3qg+dnmJwKSQZlwByHCSpqf431Agh2G6u+Zq6Ij23Xf2fI/Z9PBs7
NInOjcsoNIAYk5Ivt2Y5ax0i/fX89dJpNQKCAQALlKJGFKu/aragLG4O8j4/Q1eQ
m4yHDiPFrmKcfLyIer6Dbn7SmRgU7XiSYwM/9JQBGE4jiBLfyBBXzb9CtFIxs08d
ljVO2GJBNm+/5gXETYeg67HERT4wBSyTFGVwnV/Ef5PwakKhCapc3bTAhm4AANdD
LL6fXzkFx6ES5WTTbIv1x1CJZdAyrRrSEn6vBwXO0xDzG4fOIPpZpIjJa7TZ9DkX
NDPnBP1019ZXz20S3ZaRKCUKTbfYqdmgiXC/PHTrga7aLuDVRG6xQ4zGIyPajot9
XMG1nnUJ4okVH0eHEmEggif92Ahm6QVAO2vQ4W72g2zh5XLcGJWqW2eqS6CM
-----END RSA PRIVATE KEY-----
EOF

chmod 400 ~/.ssh/id_rsa
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa

cat >> ~/.ssh/authorized_keys << EOF
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDD7TwFX4ADYyV1mDW+kUyu7WMSz+F2JfmPsEkXeH/LwBk/eXsFH4q9GPLwIr7MPmywxS22PpccTdj6zFvJsteqIfuApdA1Ko4x2jmXod1aWvktoTU/9VKkciMCWeOpr9aSSNvqCIv4mV+z2qz03490coTVTd25ismddu71uahmolWUffQb0ircQ9nbWSR6rSWW86s0npEf5UviaWoBaJMotMbUoc6mi1YTyMix5dZjVdj4fXCg090QQTLxdWiam41uoTVuc7+3mVP8SzPi5jTWkMlO+SGqJM8+zXCaAkCEtXXEc6QYj/nu1Y4iO9CLT2orUiY2yAx4FW5mgD+OXc64Vhu7BBHxW/rYBflfiG5lhngb6VaOKU3NZTdtvIHRQ46sBA4ZOyIUj7tKiPDO05GMqrQM9/7BdtOAUoQg/MX4x7xUWjMLdgM5fodBHHpsCZJopxkF5MiSDnl2NExBvfnaO5Z1xZA+DKYVZy0twSfMVGQ42F5LqsAfddd6Sp2zGEsBktOGFtCgWRobqwWTnEYjJT22rbPAjSJIpxUlpvFxNfyDAOoUIaEffUHBrMgpJXjUZ51J372aL5Bplt5Jnr0CRi4g/FL7MTpzc7CkdipQB0dGA/QUCrvQqwuO/eZdmSZCStf9qXKQ6/EeCeEVBob0DDpobFHsiE469pfgcAOuZQ== licai@cs.utah.edu
EOF

cd $WORKINGDIR
git clone git@gitlab.flux.utah.edu:licai/deepstitch.git
pushd $KUBEHOME
git clone https://github.com/microservices-demo/microservices-demo
popd
git clone git@gitlab.flux.utah.edu:licai/emulab-profile.git

sudo apt-get update && sudo apt-get install -y apt-transport-https curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl

#sudo apt-get -y install docker-engine
# here we see the ports are not open, kubelet is not started somehow.

# used this to look at the log:  journalctl -xeu kubelet 
# failed to run Kubelet: failed to create kubelet: docker API version is older than 1.26.0
curl -fsSL get.docker.com | sh
sudo apt -y install docker.io
sudo docker version
sudo swapoff -a
sudo systemctl enable docker.service

sudo kubeadm init --pod-network-cidr=192.168.0.0/16
# result will be like:  kubeadm join 10.11.10.7:6443 --token vgh27g.b87fxxsykyfscq4g \
    --discovery-token-ca-cert-hash sha256:61caa9914d3ae7e085ae864c4ab2071caea1c949e56e806f788eb95c7ff7366e 


sudo apt-get -y install build-essential libffi-dev python python-dev  \
python-pip automake autoconf libtool indent vim tmux exuberant-ctags  \
python3-dev 

curl https://bootstrap.pypa.io/get-pip.py | sudo python3

# learn from this: https://blog.csdn.net/yan234280533/article/details/75136630
# more info should see: https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/
sudo apt-get -y install kubelet kubeadm kubectl kubernetes-cni golang-go jq

# admin.conf is needed otherwise you will see this:
# The connection to the server localhost:8080 was refused - did you specify the right host or port?
# https://github.com/kubernetes/kubernetes/issues/44665
sudo cp /etc/kubernetes/admin.conf $KUBEHOME/
sudo chown ${username}:${usergid} $KUBEHOME/admin.conf

#kubectl -n kube-system get cm kubeadm-config -oyaml | sed 's#/etc/resolv.conf#/dev/null#g' | kubectl replace -f -
#kubectl -n kube-system get cm kubelet-config-1.11 -oyaml | sed 's#/etc/resolv.conf#/dev/null#g' | kubectl replace -f -
#sudo sed -i 's#/etc/resolv.conf#/dev/null#g' /var/lib/kubelet/config.yaml

kubectl create -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/k8s-manifests/kube-flannel-rbac.yml
kubectl create -f https://github.com/coreos/flannel/raw/master/Documentation/kube-flannel.yml
# use this to enable autocomplete
source <(kubectl completion bash)

# kubectl get nodes --kubeconfig=${KUBEHOME}/admin.conf -s https://155.98.36.111:6443
# Install dashboard: https://github.com/kubernetes/dashboard
#sudo kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml
 
# run the proxy to make the dashboard portal accessible from outside
sudo kubectl proxy  --kubeconfig=${KUBEHOME}/admin.conf  &

# https://github.com/kubernetes/dashboard/wiki/Creating-sample-user
kubectl create -f $DEPLOY_CONFIG/create-cluster-role-binding-admin.yaml  
kubectl create -f $DEPLOY_CONFIG/create-service-account-admin-uesr-dashboard.yaml
# to print the token, use this cmd below to paste into the browser.
# kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}') |grep token: | awk '{print $2}'

# jid for json parsing.
export GOPATH=${WORKINGDIR}/go/gopath
mkdir -p $GOPATH
export PATH=$PATH:$GOPATH/bin
sudo go get -u github.com/simeji/jid/cmd/jid
sudo go build -o /usr/bin/jid github.com/simeji/jid/cmd/jid

sudo pip install kubernetes docker psutil
sudo pip3 install kubernetes docker psutil numpy matplotlib keras tensorflow

################################
# get host count
h_index=0
ret_errno=0
while [ $ret_errno -eq 0 ]
do 
    h_index=$((h_index + 1))
    ping -c 1 h-$h_index
    ret_errno=$?
done
h_index=$((h_index - 1))
echo $h_index
################################


# Wait till the slave nodes get joined and update the kubelet daemon successfully
node_cnt=$h_index
joined_cnt=$(( `kubectl get nodes |wc -l` - 1 ))
while [ $node_cnt -ne $joined_cnt ]
do 
    joined_cnt=$(( `kubectl get nodes |wc -l` - 1 ))
    sleep 5
done
sleep 1m
    
###############################################################3
# install microservices app
kubectl apply -f  ${WORKINGDIR}/deepstitch/kubernetes/microservices-yaml/manifests/sock-shop-ns.yaml 
sleep 5s
kubectl apply -f  ${WORKINGDIR}/deepstitch/kubernetes/microservices-yaml/manifests
sleep 5s
kubectl apply -f  ${WORKINGDIR}/deepstitch/kubernetes/microservices-yaml/manifests-jaeger
sleep 5m

#$ kubectl get endpoints --all-namespaces |grep jaeger-query|awk '{print $3}'
#192.168.1.14:16686
#$ kubectl get endpoints --all-namespaces |grep front-end|awk '{print $3}'
#192.168.1.5:8079
#$ kubectl get endpoints --all-namespaces |grep dashboard|awk '{print $3}'
#192.168.0.2:8443
#jaeger_access=`kubectl get endpoints -n jaeger  -o go-template='{{range .items}}{{if eq .metadata.name "jaeger-query"}} {{index .subsets 0 "addresses" 0 "ip" }}|{{index .subsets 0 "ports" 0 "port"}} {{end}}{{end}}' | tr "|" ":"`

jaeger_endpoint=`kubectl get endpoints --all-namespaces |grep jaeger-query|awk '{print $3}'`
sockshop_endpoint=`kubectl get endpoints --all-namespaces |grep front-end|awk '{print $3}'`
dashboard_endpoint=`kubectl get endpoints --all-namespaces |grep dashboard|awk '{print $3}'`
dashboard_credential=`kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}') |grep token: | awk '{print $2}'`
jaeger_port=`kubectl --namespace=jaeger get svc -o go-template='{{range .items}}{{if eq .metadata.name "jaeger-query"}}{{index .spec.ports 0 "nodePort"}}{{"\n"}}{{end}}{{end}}'`

echo "Kubernetes is ready at: http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/login"
echo "sockshop will be ready in 5 minutes at: http://localhost:30001"
echo "jaeger will be ready in 5 minutes at: http://localhost:${jaeger_port}"

# optional address
echo "Or, another access option (jaeger's localhost port does not work on my windows port forwarding somehow)"
echo "Jaeger endpoint: $jaeger_endpoint"
echo "sockshop endpoint: $sockshop_endpoint"
echo "kubernetes dashboard endpoint: $dashboard_endpoint"
# dashboard credential
echo "And this is the dashboard credential: $dashboard_credential"

# to know how much time it takes to instantiate everything.
date

# because on ubuntu 18.04 docker-engine somehow does not work very well with kubernetes, I may either choose docker-ce or fall back to ubuntu1604. 
# But somehow the 18.04 configuration using daemon does not work properly somehow. The four microservices can not boot up correctly.
# so just roll back.
sudo apt-get -y install linux-image-4.13.0-16-generic linux-tools-4.13.0-16-generic linux-headers-4.13.0-16-generic
#sudo reboot
     
