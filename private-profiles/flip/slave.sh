#!/bin/bash
#set -u
#set -x
# deploy sgx on emulab
SCRIPTDIR=$(dirname "$0")
WORKINGDIR='/mnt/extra/'
username=$(id -u)
usergid=$(id -g)

KUBEHOME="${WORKINGDIR}/kube/"
sudo mkdir ${WORKINGDIR} 
sudo mkdir -p $KUBEHOME && cd $KUBEHOME

sudo chown ${username}:${usergid} ${WORKINGDIR}/ -R
cd $WORKINGDIR
exec >> ${WORKINGDIR}/deploy.log
exec 2>&1

export KUBECONFIG=$KUBEHOME/admin.conf

cd
mkdir .ssh
cat >> .ssh/id_rsa << EOF
-----BEGIN RSA PRIVATE KEY-----
MIIJKQIBAAKCAgEAw+08BV+AA2MldZg1vpFMru1jEs/hdiX5j7BJF3h/y8AZP3l7
BR+KvRjy8CK+zD5ssMUttj6XHE3Y+sxbybLXqiH7gKXQNSqOMdo5l6HdWlr5LaE1
P/VSpHIjAlnjqa/Wkkjb6giL+Jlfs9qs9N+PdHKE1U3duYrJnXbu9bmoZqJVlH30
G9Iq3EPZ21kkeq0llvOrNJ6RH+VL4mlqAWiTKLTG1KHOpotWE8jIseXWY1XY+H1w
oNPdEEEy8XVompuNbqE1bnO/t5lT/Esz4uY01pDJTvkhqiTPPs1wmgJAhLV1xHOk
GI/57tWOIjvQi09qK1ImNsgMeBVuZoA/jl3OuFYbuwQR8Vv62AX5X4huZYZ4G+lW
jilNzWU3bbyB0UOOrAQOGTsiFI+7SojwztORjKq0DPf+wXbTgFKEIPzF+Me8VFoz
C3YDOX6HQRx6bAmSaKcZBeTIkg55djRMQb352juWdcWQPgymFWctLcEnzFRkONhe
S6rAH3XXekqdsxhLAZLThhbQoFkaG6sFk5xGIyU9tq2zwI0iSKcVJabxcTX8gwDq
FCGhH31BwazIKSV41GedSd+9mi+QaZbeSZ69AkYuIPxS+zE6c3OwpHYqUAdHRgP0
FAq70KsLjv3mXZkmQkrX/alykOvxHgnhFQaG9Aw6aGxR7IhOOvaX4HADrmUCAwEA
AQKCAgBs9FeFcWdB+vfQ3lU85ZZsTMzkoD/0MQgIVPNAnx0KVIh12PuFjNXddKtc
ySWrdSeCYhSanVHxRoVcaxq8Hr15+i3fi9YuSr0Kkh2m6jerUJ3zNwvixm0vz90S
KKTHL2a2sNfC3P/hvoEPBpNaFZy0GB8wc33S+ZBHBeW7NoSklq/axPRkKEr3dLAJ
IBh+NB2MRXM27tsrjprVjOOLZvk3ENjlVBoUWbYDiIu7CmJfmVTwUum4NjlgGbHX
vv/twoUzOWj6RFogIp8m1TdEfdpyW8/nH/oNoSyy8xIkQw5X/oVAO0LM+fv2SkIh
XXwKjJ4w+x0PJ85vVROw4q71X8tdlCZ98/x9rGRXggK0QIHzStdbEYq9xqMo4lsE
WGzPGWQxgr8jo7kO396jCw8KJONqHBkJSQ0cYnQ86v10ev21A+q0KzGP5YPxuqOd
g476Gxgmt0clvXVTlgoMsuyrUNDHqnKAgcc+/uulnf37U596ezmYKJnL8ewsJrA8
R0GFs9mRrngw+WqjJsm5Y/M7EOdEk+mrPlMnYNK5GpjO5SOyyeCB2yuB8UOtMJll
769i+gDIRJcoqEKdSXoPSrSr5/UaXbDuhBknYiZM9vcyJ+dD2vQ22H8kUPKcLJXU
5PUZ0z/oFP4ZxHTgma8Ad5Kj+Fxn9FR8P7jy4yQllS8s4oA3HQKCAQEA5yPhdSqj
VGzVuAE+BqDh+Q05caJg1c9QXZJit9DnS7bjyqmBeSzD47U15Zxkh3iNPwn+cS/i
isv7E6a7G9bSrl5+5MnlPFRVlUFZ1aBXMswdpOP+V9eh20/DlOsgNc/dlxuSOVcM
zL6wJdy6K8TdAscCJKvI4eNpCWnpYaKNgD2lNqGb7cQ4FInxVWKJhl/Z8PQz2OOO
kt0taD3V3QKXKsTBUthjxuBAIfBbrE93bjw1lWmZTfuBm9hjL5q0PYYfjHY3M7IK
GPvMHncNtB2PN62X+3gcAUGWVwWKLe23x7GLc6PO1xe3vC/C9FX4OSuXCN3UkBYJ
NgpMEFt+sIkZPwKCAQEA2P/M9AsGkoJM25r8ryvUkJ+BEdFjiXK0e6MrbqpUGLxN
xv2rmF6edZpgvcEoHzrI2GYveEk+qoCjOT4ndvqDmhpAqfmv9sVdw68HHA52vjAS
sSJCrwlchM05ksLfSFAG6ty3ZCFqsWs3as1weXzE+LNA0Mc1Ja4iUUiP9yLpX0Xj
c2zYZhBqByaVBYjcGoiCJhp35QBerik/pmv4W6BkGFR+qd2zjVocblCEQwnPrbpE
1JSEicihu4exSrJNkOqOuTe0XSNtskLqpQ4HzJG5e6qqextJuMamtFq/1d1cI1U1
Pkp6ytzKSocTAs8TXll4uczdG6ltaT3WizlTZk4LWwKCAQEAwTVu+r+/c4zeQUdY
j7Xf7o0tXV18UH1REkaRGlIXZWTlmLyEK8RNooQRH3PRXKRafT1fYUHDvn2vzMds
VGMIDRWUQK67Xeio+XB9JWM4QI/6n6CtNMRc2bQdKtg4PoMZGXzEiAXJ/0POfne5
b2jJzc6Ibhriel/bpzDPEvwMCAx/k+EXNHL25cRA5TzO9HIe/ZlD1aKxxPX5RM+b
e3j0351EdHnJPa6qLTR7jNeIX68Yo2J38YWw25LPpjVNQLhimskeb6n9YtDn2Pdv
jwW+d642AtBtqGcowmkxDh9NpjE0/l8JQSbkQPwznIZYTYGwaIr1adPhszh8SJgS
mIKhhQKCAQEA0LzwKGlavyA8Ups6Kj8dUKk+fSdQem0ulCgXpXKPRuQMZTirEf0i
rLn+CUp/D9t6TD+vtiOu9lCbsnrJd7fvVBOg1GDtFZIxagU575EyRCz2LhJo1vtI
Vi8cDMMMCgGBRfdfKiDDAzvBqSlJ4nNU/m9gD/UweLiTJmGIqxCYMR+Hdgbj+vob
JBnHC/Xk1Fkv6PtZDZ5XdW0UX0BJPcqBvq240ioDiBuAcdVeBSaw8crWXy8md2vZ
uA0FTHdMCfS3qg+dnmJwKSQZlwByHCSpqf431Agh2G6u+Zq6Ij23Xf2fI/Z9PBs7
NInOjcsoNIAYk5Ivt2Y5ax0i/fX89dJpNQKCAQALlKJGFKu/aragLG4O8j4/Q1eQ
m4yHDiPFrmKcfLyIer6Dbn7SmRgU7XiSYwM/9JQBGE4jiBLfyBBXzb9CtFIxs08d
ljVO2GJBNm+/5gXETYeg67HERT4wBSyTFGVwnV/Ef5PwakKhCapc3bTAhm4AANdD
LL6fXzkFx6ES5WTTbIv1x1CJZdAyrRrSEn6vBwXO0xDzG4fOIPpZpIjJa7TZ9DkX
NDPnBP1019ZXz20S3ZaRKCUKTbfYqdmgiXC/PHTrga7aLuDVRG6xQ4zGIyPajot9
XMG1nnUJ4okVH0eHEmEggif92Ahm6QVAO2vQ4W72g2zh5XLcGJWqW2eqS6CM
-----END RSA PRIVATE KEY-----
EOF

chmod 400 ~/.ssh/id_rsa
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa

cat >> ~/.ssh/authorized_keys << EOF
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDD7TwFX4ADYyV1mDW+kUyu7WMSz+F2JfmPsEkXeH/LwBk/eXsFH4q9GPLwIr7MPmywxS22PpccTdj6zFvJsteqIfuApdA1Ko4x2jmXod1aWvktoTU/9VKkciMCWeOpr9aSSNvqCIv4mV+z2qz03490coTVTd25ismddu71uahmolWUffQb0ircQ9nbWSR6rSWW86s0npEf5UviaWoBaJMotMbUoc6mi1YTyMix5dZjVdj4fXCg090QQTLxdWiam41uoTVuc7+3mVP8SzPi5jTWkMlO+SGqJM8+zXCaAkCEtXXEc6QYj/nu1Y4iO9CLT2orUiY2yAx4FW5mgD+OXc64Vhu7BBHxW/rYBflfiG5lhngb6VaOKU3NZTdtvIHRQ46sBA4ZOyIUj7tKiPDO05GMqrQM9/7BdtOAUoQg/MX4x7xUWjMLdgM5fodBHHpsCZJopxkF5MiSDnl2NExBvfnaO5Z1xZA+DKYVZy0twSfMVGQ42F5LqsAfddd6Sp2zGEsBktOGFtCgWRobqwWTnEYjJT22rbPAjSJIpxUlpvFxNfyDAOoUIaEffUHBrMgpJXjUZ51J372aL5Bplt5Jnr0CRi4g/FL7MTpzc7CkdipQB0dGA/QUCrvQqwuO/eZdmSZCStf9qXKQ6/EeCeEVBob0DDpobFHsiE469pfgcAOuZQ== licai@cs.utah.edu
EOF

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list

cd $WORKINGDIR
git clone git@gitlab.flux.utah.edu:licai/deepstitch.git

sudo apt-get update
sudo apt-get -y install build-essential libffi-dev python python-dev  \
python-pip automake autoconf libtool indent vim tmux jq exuberant-ctags \
python3-dev

curl https://bootstrap.pypa.io/get-pip.py | sudo python3

# learn from this: https://blog.csdn.net/yan234280533/article/details/75136630
sudo apt-get -y install kubelet kubeadm kubectl kubernetes-cni golang-go jq
#sudo apt-get -y install docker-engine
# this to install docker-ce, instead of docker-engine
# curl -fsSL get.docker.com | sh
sudo apt -y install docker.io
sudo systemctl enable docker.service
sudo docker version
sudo swapoff -a

master_token=''
#patched=1
while [[ -z $master_token ]]
#while [[ -z $master_token ]] || [[ $patched == 1 ]]
do
    #patched=`ssh -o StrictHostKeyChecking=no m "cat /var/lib/kubelet/config.yaml |grep resolv.conf |wc -l"`
    master_token=`ssh -o StrictHostKeyChecking=no m "export KUBECONFIG='/mnt/extra/kube/admin.conf' &&   kubeadm token list |grep authentication | cut -d' ' -f 1"`;
    sleep 1;
done
sudo kubeadm join m:6443 --token $master_token --discovery-token-unsafe-skip-ca-verification 
scp m:$KUBEHOME/admin.conf $KUBEHOME/admin.conf


###############################################################

# patch the kubelet to force --resolv-conf=''
sudo sed -i 's#Environment="KUBELET_CONFIG_ARGS=--config=/var/lib/kubelet/config.yaml"#Environment="KUBELET_CONFIG_ARGS=--config=/var/lib/kubelet/config.yaml --resolv-conf=''"#g' /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
sudo systemctl daemon-reload 
sudo systemctl restart kubelet.service

# if it complains that "[ERROR Port-10250]: Port 10250 is in use", kill the process.
# if it complains some file already exist, remove those. [ERROR FileAvailable--etc-kubernetes-pki-ca.crt]: /etc/kubernetes/pki/ca.crt already exists

date

# because on ubuntu 18.04 docker-engine somehow does not work very well with kubernetes, I may either choose docker-ce or fall back to ubuntu1604. 
# But somehow the 18.04 configuration using daemon does not work properly somehow. The four microservices can not boot up correctly.
# so just roll back.
sudo apt-get -y install linux-image-4.13.0-16-generic linux-tools-4.13.0-16-generic linux-headers-4.13.0-16-generic
sudo pip install kubernetes docker psutil
sudo pip3 install kubernetes docker psutil numpy matplotlib keras tensorflow

