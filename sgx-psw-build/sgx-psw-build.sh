#!/bin/bash

# Deploy sgx on emulab
# This script is writen based on https://github.com/01org/linux-sgx
# Last update: 2017-7-6 
# With git commit id: 8383b8c65f49fca017c62eb7efdd97a212829caf

#set -u
#set -x

SCRIPTDIR=$(dirname "$0")
WORKINGDIR='/mnt/extra'
SGXSRC='/mnt/extra/sgxsrc'
SGXSDKROOT='/mnt/extra/sgxsdk'
username=`whoami`
usergid=`id -g $username`

sudo chown ${username}:${usergid} /mnt/extra/ -R
cd $WORKINGDIR
exec >> ${WORKINGDIR}/deploy.log
exec 2>&1

sudo apt-get update
sudo apt-get -y install build-essential ocaml automake autoconf libtool wget python vim screen libssl-dev libcurl4-openssl-dev protobuf-compiler libprotobuf-dev alien

# compile and install sgx driver
cd $WORKINGDIR
git clone https://github.com/01org/linux-sgx-driver
cd linux-sgx-driver
make
sudo mkdir -p "/lib/modules/"`uname -r`"/kernel/drivers/intel/sgx"    
sudo cp isgx.ko "/lib/modules/"`uname -r`"/kernel/drivers/intel/sgx"    
sudo sh -c "cat /etc/modules | grep -Fxq isgx || echo isgx >> /etc/modules"    
sudo /sbin/depmod
sudo /sbin/modprobe isgx

# compile sgx sdk and psw
cd $WORKINGDIR
git clone https://github.com/01org/linux-sgx.git $SGXSRC
cd $SGXSRC
./download_prebuilt.sh

sudo make DEBUG=1 
sudo make DEBUG=1 sdk_install_pkg
sudo make psw_install_pkg DEBUG=1 
cd $WORKINGDIR
printf "yes\n" | sudo $SGXSRC/linux/installer/bin/sgx_linux_x64_sdk_*.bin

## install something to use Trusted Platform Service functions.
## Although I am not quite clear what's that used for :(
if [ -e "/dev/mei0" ]; then
    cd $WORKINGDIR
    mkdir iCLS
    cd iCLS
    wget http://registrationcenter-download.intel.com/akdlm/irc_nas/11414/iclsClient-1.45.449.12-1.x86_64.rpm
    if [ -f iclsClient-1.45.449.12-1.x86_64.rpm ]; then
        sudo alien --scripts iclsClient-1.45.449.12-1.x86_64.rpm
        sudo dpkg -i iclsclient_1.45.449.12-2_amd64.deb
        sudo apt-get -y install uuid-dev libxml2-dev cmake pkg-config
        git clone https://github.com/01org/dynamic-application-loader-host-interface
        cd dynamic-application-loader-host-interface
        cmake .
        make
        sudo make install
        sudo systemctl enable jhi
    else
        echo "\n\n**Note: iCLS file does not exist, thus not installed.**"
        echo "\n\n**      Check whether it is because of version changed.**"
    fi
else
    echo "\n\n**Note: /dev/mei0 does not exist, thus not installed.**"
fi

cd $WORKINGDIR
printf "yes\n" | sudo $SGXSRC/linux/installer/bin/sgx_linux_x64_psw_*.bin

# test whether it is properly installed
source $SGXSDKROOT/environment
cd $SGXSDKROOT/SampleCode/LocalAttestation

# the make is going hardware debug mode by default
sudo make
echo "\n\nTesting SGX with LocalAttestation Sample Code\n\n"
./app

   

echo ""
echo ""
echo "###################################################"
echo "Please checkout these directories:"
echo "    SGX source dir: $SGXSRC"
echo "    SGX SDK dir: $SGXSDKROOT"
echo "    To finish run sgx program run:"
echo "      \`source $SGXSDKROOT/environment\`"
echo "    Try executing commands below to verify it works: "
echo "      cd $SGXSDKROOT/SampleCode/LocalAttestation"
echo "      ./app"
echo "    The psw is installed at /opt/intel/sgxpsw"
