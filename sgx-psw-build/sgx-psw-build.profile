""" Intel SGX Hardware driver and sdk prebuilt
It just scripts out the building instruction of [building linux-sgx](https://github.com/01org/linux-sgx)

For more information, please read the build script at /mnt/extra/sgx-psw-build.sh after it is instantiated.
Something to pay attention to:
1. Currently Emulab only has one physical machine with SGX enabled in the BIOS.
2. **Temporary** local storage is at /mnt/extra
3. ubuntu 16.04<br>

Instructions:
###Note:
when you instantiate the profile, after the host is booted.<br>
You can see the experiment "State" is **"booted (startup services are still running)"** for a while.<br>
This is because the SGX building procedure is running.<br>
So make sure you login after the "State" change into **"ready"**

Once it is instantiated, login and run:\n
```shell-script
    source /mnt/extra/sgxsdk/environment
```

You can find \n
    SGX source is at: /mnt/extra/sgxsrc
    SGX sdk is at: /mnt/extra/sgxsdk

You can also try out the remote attestation code: 
```shell-script
    source /mnt/extra/sgxsdk/environment \n
    cd /mnt/extra/sgxsdk/SampleCode/LocalAttestation \n
    ./app
```

For deployment debugging, please check /mnt/extra/debug.log

"""

#
# NOTE: This code was machine converted. An actual human would not
#       write code like this!
#

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import the Emulab specific extensions.
import geni.rspec.emulab as emulab

# Create a portal object,
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Node h
node_h = request.RawPC('h')
node_h.hardware_type = "nuc6260"
node_h.disk_image = 'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU16-64-STD'
bs = node_h.Blockstore('bs', '/mnt/extra')
bs.size = '225GB'

node_h.addService(pg.Install('https://gitlab.flux.utah.edu/licai/emulab-profile/raw/master/sgx-psw-build/sgx-psw-build.tar.gz','/mnt/extra/'))
node_h.addService(pg.Execute(shell="bash", command="/mnt/extra/sgx-psw-build.sh"))

# Print the generated rspec
pc.printRequestRSpec(request)

#
# Here is the original NS file from the Emulab experiment.
#
# set ns [new Simulator]
# $ns description "Used for data analysis"
# source tb_compat.tcl
# 
# set h [$ns node]
# 
# tb-set-hardware $h d430
# 
# tb-set-node-os $h UBUNTU14-64-STD
# 
# set bs1 [$ns blockstore]
# $bs1 set-class "local"
# $bs1 set-size "100GB"
# $bs1 set-placement "nonsysvol"
# $bs1 set-mount-point "/mnt/extra"
# $bs1 set-node $h
# 
# $ns rtproto Static
# 
# # Go!
# $ns run
