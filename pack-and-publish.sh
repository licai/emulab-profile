#!/bin/bash
#set -x
set -u

PROFILE_NAME=""
TARGET_TARBALL=""
TARGET_SCRIPT=""
currentdir=`printf '%s\n' "${PWD##*/}"`

function script_usage(){
    #set +x
    echo "Usage: $0"
    echo "Note: Make sure '`pwd`' is the same directory with the target profile"
    exit -1
}

function pack_it(){
    echo $TARGET_TARBALL
    if [ -f $TARGET_TARBALL ]; then
        rm $TARGET_TARBALL
    fi
    touch $TARGET_TARBALL
    tar czf $TARGET_TARBALL $TARGET_SCRIPT
}

function push_it(){
    git add $TARGET_TARBALL $TARGET_SCRIPT
    git commit -m "Update script and tarball for profile $PROFILE_NAME"
    #git push origin master
}

function pack-and-publish(){
    if [ -r "$TARGET_SCRIPT" ]; then
        pack_it
        push_it
    else
        echo "ERROR: script is empty or not exist."
    fi
}

if [[  "$BASH_SOURCE" == "$0" ]]; then
    echo "Argument checking for $0"
    echo "Argument count $#"
    if [ "$#" -eq 0 ]; then
        PROFILE_NAME=$currentdir
        TARGET_TARBALL="${PROFILE_NAME}.tar.gz"
        TARGET_SCRIPT="${PROFILE_NAME}.sh"
    else
        script_usage
    fi
    pack-and-publish
fi 
